#!/usr/bin/perl -s -ln
# worklog parser

# --------------------------------------------------------------------
# Synopsis: Personal work log filter, for time tracking work tasks.
#   Keywords: Time billing, time accounting, time card, project plan.
# --------------------------------------------------------------------
# Usage: ourname [-v] < WORKLOG
#   ex entries:
#     websos += 103 + 51  # default unit is minutes. comments ok, the're stripped
#     wklog += 5.3h
#     wklog += 53m
# --------------------------------------------------------------------
# Rating: tone: tool used: rarely provincial: y stable: n TBDs: y 
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2009,2010 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# -- Software License --
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---

$verbose = 1 if ($v);

s~^\s*\d{6}:\d\d:\d\d: ~~;
  # remove leading "doing journal" time stamps as in
  # 100128:13:36: 

if(m{^\s*[\w-]+\s*\+=\s*-?\d+}) {
  # supported:   foo+=-45
  # \w == wordchar == [0-9a-z_A-Z]
  # supported:   svn-rs+=-45
  print "o: $_" if ($verbose);
  s~\s+#.*~~;
    # strip comments
  s~(\d+)(\s*m)~$1~g;
    # strip off m, minutes are assumed now
  s~(\d+)(\s*h)~$1*60~g;
    # convert for Ex 1.25h to 1.25 * 60
    #   corner cases: +20h +2.2h -33h

  print "n: $_\n" if ($verbose);
  if (m{^\s*([\w-]+)\s*\+=(.*)})
  {
    $tot{$1} += eval $2;
  }
  else
  {
    print STDERR "ERROR/oops";
  }

}

END {
  foreach (sort keys %tot)
  {
    $tot_min = $tot{$_};
    $Grand_tot += $tot_min;
    $remainder = $tot_min % 60;
    $tot_hours = sprintf ( "%d", $tot_min/60);

    printf "%14s: %6sm %4sh %2sm %7.2fh\n", $_, $tot_min, $tot_hours, $remainder, $tot_min/60;
  }
  printf "\ngrand total hr: %7.2fh\n", $Grand_tot/60;
}
