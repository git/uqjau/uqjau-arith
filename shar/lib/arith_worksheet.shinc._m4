#!/bin/false -bash_code_meanttobesourced

# ====================================================================
# Copyright (c) 2017 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

__calc() {
    <<\____eohd
    
    Running total in $_calcSum

    -c == clear $_calcSum
    -q == echo nothing, just update total
    -S == do not update total
    -s SCALE 
        for 'bc'

____eohd

    local opt_true=1 opt_char badOpt=
    OPTIND=1

    local OPT_s= OPT_S= OPT_c= OPT_q=
    while getopts s:Scq opt_char
    do
        # save info in an "OPT_*" env var.
        [[ $opt_char != \? ]] && eval OPT_${opt_char}="\"\${OPTARG:-$opt_true}\"" ||
            badOpt=1
    done
    shift $(( $OPTIND -1 ))

    [[ -z $badOpt ]] || { return 1 ; }

    [[ -z $OPT_c ]]  || { _calcSum=; return 0; }

    local results=$(
        {
           echo scale=${OPT_s:-3}
           echo "$@" |tr -d \\n  # support '\n' in args for long lines
           printf "\n"
        } | command bc -lq || true
    )

    # Keep a running total in $_calcSum
    if [[ -z $OPT_S ]] ;then
        _calcSum=$( __calc -S ${_calcSum:-0} + $results )
    fi

    [[ -n $OPT_q ]] || echo $results

    : :end '__calc()'
}

_sum() {
    __calc -q "$@"
}

addTo() {
    local totalVarName=$1 # Refers to a global var.

    eval $totalVarName=$( eval _calc \${$totalVarName:-0} + $2 )
}

